var FOURSQUARE_API = "https://api.foursquare.com/v2/";

Date.prototype.YYYYmmdd = function() {
   var yyyy = this.getFullYear().toString();
   var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
   var dd  = this.getDate().toString();
   return yyyy + (mm[1]?mm:"0"+mm[0]) + (dd[1]?dd:"0"+dd[0]); // padding
};
d = new Date();

module.exports.get_venue = function(credentials, criteria , callback) {

	var url = FOURSQUARE_API + "venues/search";
	var xhr = Ti.Network.createHTTPClient();

	xhr.onerror = function(e){
		alert('Error' + e.error);
	};

	xhr.onload = function(e){
		obj = JSON.parse(this.responseText);
		var result = [];

		for (var index = 0 ; index < obj.response.venues.length; index++){
			var venue = obj.response.venues[index];
			
			result.push({
					id : venue.id,
					name : venue.name,
					location : venue.location
				});
		}

		callback(result);
	};
	
	var params = criteria + '&client_id=' + credentials.CLIENT_ID +
	 '&client_secret=' + credentials.CLIENT_SECRET + '&v=' + d.YYYYmmdd();

	if(Ti.Network.online){

		Ti.API.info("url with params //: " + url + params);
		xhr.open('GET', url + params);
		xhr.send();
	} else {
		// alert the user that he/she is
		// not currently online
	}
}

module.exports.get_details = function(credentials, criteria, callback) {

	var url = FOURSQUARE_API + "venues/";
	var xhr = Ti.Network.createHTTPClient();

	xhr.onerror = function(e){
		alert('Error' + e.error);
	};

	xhr.onload = function(e){

		obj = JSON.parse(this.responseText);

		/**	
			Foursquare api version 2 is used here..
		**/
		var _id = obj.response.venue.id;
		var _name = obj.response.venue.name;
		var _contact = obj.response.venue.contact;
		var _url = obj.response.venue.url;
		var _menu = obj.response.venue.menu;
		var _location = obj.response.venue.location;
		var _photos = obj.response.venue.photos;

		callback({
			name : _name,
			contact : _contact,
			url : _url,
			menu : _menu,
			location : _location,
			photos : _photos
		});
	};
	
	var params = criteria + '?client_id=' + credentials.CLIENT_ID +
	 '&client_secret=' + credentials.CLIENT_SECRET + '&v=' + d.YYYYmmdd();

	if(Ti.Network.online){
		Ti.API.info("url with params //: " + url + params);
		xhr.cache = true;
		xhr.open('GET', url + params);
		xhr.send();
	} else {
		// alert the user that he/she is
		// not currently online
	}
}