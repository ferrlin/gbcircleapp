// The contents of this file will be executed before any of
// your view controllers are ever executed, including the index.
// You have access to all functionality on the `Alloy` namespace.
//
// This is a great place to do any initialization for your app
// or create any global variables/functions that you'd like to
// make available throughout your app. You can easily make things
// accessible globally by attaching them to the `Alloy.Globals`
// object. For example:
//
var TIMEOUT = 4000;

if( Titanium.App.deployType !== 'production')
{
	var behave = require('behave');
	//require your created specs
	require('spec/test-spec-1');
	// run : tests
	behave.run();
}

// Foursquare Credentials
Alloy.Globals.F4SQR_CREDS = {
	CLIENT_ID: 'PZRV5T2RB0GMW2LK04LQ2O4CQRGZ5CEV1ECO0THKFLOIDW5V',
	CLIENT_SECRET: 'UAW1N52BL2XEBTTV02S1C3G430AU1UTNB12YQU4RBOUDMPDZ'
}

/*
	Foursquare High Level categories:
*/

Alloy.Globals.BGCApp_CATEGORIES = [
{
	title: "Buses",
	id: "4bf58dd8d48988d1fe931735",
	name: "Bus Station",
	icon:{
		prefix: "https://foursquare.com/img/categories_v2/travel/busstation_",
	suffix: ".png"
	},
	color : '#009cab',
    font : {
        fontSize: '21dp',
        fontWeight: 'bold',
        fontFamily: 'Century Gothic'
    }
},
{
	title: "Dining",
	id: "4bf58dd8d48988d1c4941735",
	name: "Restaurant",	
	icon: {
		prefix: "https://foursquare.com/img/categories_v2/food/default_",
		suffix: ".png"
	},
	color : '#009cab',
    font : {
        fontSize: '21dp',
        fontWeight: 'bold',
        fontFamily: 'Century Gothic'
    }
},
{
	title: "Coffee Shop",
	id: "4bf58dd8d48988d1e0931735",
	name: "Coffee Shop",
	icon: {
		prefix: "https://foursquare.com/img/categories_v2/food/coffeeshop_",
		suffix: ".png"
	},
	color : '#009cab',
	font : {
		fontSize : '21dp',
		fontWeight: 'bold',
		fontFamily: 'Century Gothic'
	}
},
{
	title: "Malls",
	id: "4bf58dd8d48988d1fd941735",
	name: "Mall",
	icon: {
		prefix: "https://foursquare.com/img/categories_v2/shops/mall_",
		suffix: ".png"
	},
	color : '#009cab',
    font : {
        fontSize: '21dp',
        fontWeight: 'bold',
        fontFamily: 'Century Gothic'
    }	
},
{
	title: "Arts, Museum, Gallery",
	id: "4d4b7104d754a06370d81259",
	name: "Arts & Entertainment",
	icon: {
		prefix: "https://foursquare.com/img/categories_v2/arts_entertainment/default_",
		suffix: ".png"
	},
	color : '#009cab',
    font : {
        fontSize: '21dp',
        fontWeight: 'bold',
        fontFamily: 'Century Gothic'
    }	
},
{
	title: "Bookstores",
	id: "4bf58dd8d48988d114951735",
	name: "Bookstore",
	icon : {
		prefix: "https://foursquare.com/img/categories_v2/shops/bookstore_",
		suffix: ".png"
	},
	color : '#009cab',
    font : {
        fontSize: '21dp',
        fontWeight: 'bold',
        fontFamily: 'Century Gothic'
    }
},
{
	title: "Banks",
	id: "4bf58dd8d48988d10a951735",
	name: "Bank",
	icon: {
		prefix: "https://foursquare.com/img/categories_v2/shops/financial_",
		suffix: ".png"
	},
	color : '#009cab',
    font : {
        fontSize: '21dp',
        fontWeight: 'bold',
        fontFamily: 'Century Gothic'
    }
},
{
	title: "Churches",
	id: "4bf58dd8d48988d131941735",
	name: "Spiritual Center",
	icon: {
		prefix: "https://foursquare.com/img/categories_v2/building/religious_",
		suffix: ".png"
	},
	color : '#009cab',
    font : {
        fontSize: '21dp',
        fontWeight: 'bold',
        fontFamily: 'Century Gothic'
    }
},
{
	title: "Schools",
	id: "4bf58dd8d48988d13b941735",
	name: "School",
	icon: {
		prefix: "https://foursquare.com/img/categories_v2/building/school_",
		suffix: ".png"
	},
	color : '#009cab',
    font : {
        fontSize: '21dp',
        fontWeight: 'bold',
        fontFamily: 'Century Gothic'
    }
},
{
	title: "Apartment/Condominiums",
	id: "4d954b06a243a5684965b473",
	name: "Residential Building (Apartment / Condo)",
	icon : {
		prefix: "https://foursquare.com/img/categories_v2/building/apartment_",
		suffix: ".png"
	},
	color : '#009cab',
    font : {
        fontSize: '21dp',
        fontWeight: 'bold',
        fontFamily: 'Century Gothic'
    }
},
{
	title: "Hotels",
	id: "4bf58dd8d48988d1fa931735",
	name: "Hotel",
	icon: {
		prefix: "https://foursquare.com/img/categories_v2/travel/hotel_",
		suffix: ".png"
	},
	color : '#009cab',
    font : {
        fontSize: '21dp',
        fontWeight: 'bold',
        fontFamily: 'Century Gothic'
    }
},
{
	title: "Sports/Recreation",
	id: "4d4b7105d754a06377d81259",
	name: "Outdoors & Recreation",
	icon : {
		prefix: "https://foursquare.com/img/categories_v2/parks_outdoors/default_",
		suffix: ".png"
	},
	color : '#009cab',
    font : {
        fontSize: '21dp',
        fontWeight: 'bold',
        fontFamily: 'Century Gothic'
    }
},
{
	title: "Fitness Club",
	id: "4bf58dd8d48988d175941735",
	name: "Gym / Fitness Center",
	icon: {
		prefix: "https://foursquare.com/img/categories_v2/building/gym_",
		suffix: ".png"
	},
	color : '#009cab',
    font : {
        fontSize: '21dp',
        fontWeight: 'bold',
        fontFamily: 'Century Gothic'
    }
},
{
	title: "Hospitals",
	id: "4bf58dd8d48988d196941735",
	name: "Hospital",
	icon: {
		prefix: "https://foursquare.com/img/categories_v2/building/medical_hospital_",
		suffix: ".png"
	},
	color : '#009cab',
    font : {
        fontSize: '21dp',
        fontWeight: 'bold',
        fontFamily: 'Century Gothic'
    }
},
{
	title: "Parting Lots",
	id: "4c38df4de52ce0d596b336e1",
	name: "Parking",
	icon: {
		prefix: "https://foursquare.com/img/categories_v2/building/parking_",
		suffix: ".png"
	},
	color : '#009cab',
    font : {
        fontSize: '21dp',
        fontWeight: 'bold',
        fontFamily: 'Century Gothic'
    }
},
{
	title : "All Categories",
	id : "",
	name : "All categories"
}];

// Seeded data for demo : Mock data
Alloy.Globals.GBC_F4SQR_DATA = {
	"venues":[{"id":"50528e12e4b08459142feef8","name":"Snapworx Digital","contact":{},"location":{"lat":14.555039146160956,"lng":121.0445820892621,"distance":17,"country":"Philippines","cc":"PH"},"canonicalUrl":"https:\/\/foursquare.com\/v\/snapworx-digital\/50528e12e4b08459142feef8","categories":[{"id":"4bf58dd8d48988d125941735","name":"Tech Startup","pluralName":"Tech Startups","shortName":"Tech Startup","icon":{"prefix":"https:\/\/foursquare.com\/img\/categories_v2\/shops\/technology_","suffix":".png"},"primary":true}],"verified":false,"restricted":true,"stats":{"checkinsCount":354,"usersCount":24,"tipCount":1},"specials":{"count":0,"items":[]},"hereNow":{"count":0,"groups":[]},"referralId":"v-1367155543"},{"id":"4f3bbdfee4b0c533fa6ff5b4","name":"BGC, The Fort","contact":{},"location":{"lat":14.55497674493301,"lng":121.04483403299757,"distance":38,"country":"Philippines","cc":"PH"},"canonicalUrl":"https:\/\/foursquare.com\/v\/bgc-the-fort\/4f3bbdfee4b0c533fa6ff5b4","categories":[{"id":"4d954b06a243a5684965b473","name":"Residential Building (Apartment \/ Condo)","pluralName":"Residential Buildings (Apartments \/ Condos)","shortName":"Apartment Building","icon":{"prefix":"https:\/\/foursquare.com\/img\/categories_v2\/building\/apartment_","suffix":".png"},"primary":true}],"verified":false,"restricted":true,"stats":{"checkinsCount":552,"usersCount":306,"tipCount":3},"specials":{"count":0,"items":[]},"hereNow":{"count":2,"groups":[{"type":"others","name":"Other people here","count":2,"items":[]}]},"referralId":"v-1367155543"},{"id":"4d9d69d0c99fb60c10dac68b","name":"Global City Innovative College","contact":{},"location":{"address":"31st St.","lat":14.55475789703072,"lng":121.04425191879272,"distance":57,"city":"Taguig City","state":"Metro Manila","country":"Philippines","cc":"PH"},"canonicalUrl":"https:\/\/foursquare.com\/v\/global-city-innovative-college\/4d9d69d0c99fb60c10dac68b","categories":[{"id":"4bf58dd8d48988d1ae941735","name":"University","pluralName":"Universities","shortName":"University","icon":{"prefix":"https:\/\/foursquare.com\/img\/categories_v2\/education\/default_","suffix":".png"},"primary":true}],"verified":false,"restricted":true,"stats":{"checkinsCount":581,"usersCount":80,"tipCount":2},"specials":{"count":0,"items":[]},"hereNow":{"count":0,"groups":[]},"referralId":"v-1367155543"},{"id":"5089aef9e4b0d26215b5d791","name":"711","contact":{},"location":{"address":"BGC","lat":14.55519083819622,"lng":121.04430215116032,"distance":26,"country":"Philippines","cc":"PH"},"canonicalUrl":"https:\/\/foursquare.com\/v\/711\/5089aef9e4b0d26215b5d791","categories":[{"id":"4d954b0ea243a5684a65b473","name":"Convenience Store","pluralName":"Convenience Stores","shortName":"Convenience Stores","icon":{"prefix":"https:\/\/foursquare.com\/img\/categories_v2\/shops\/conveniencestore_","suffix":".png"},"primary":true}],"verified":false,"restricted":true,"stats":{"checkinsCount":38,"usersCount":20,"tipCount":0},"specials":{"count":0,"items":[]},"hereNow":{"count":0,"groups":[]},"referralId":"v-1367155543"},{"id":"4f164c92e4b03802078c85ec","name":"Harvest","contact":{},"location":{"address":"Bonifacio Global City","lat":14.554568575994036,"lng":121.04439733018789,"distance":70,"country":"Philippines","cc":"PH"},"canonicalUrl":"https:\/\/foursquare.com\/v\/harvest\/4f164c92e4b03802078c85ec","categories":[{"id":"4bf58dd8d48988d16d941735","name":"Café","pluralName":"Cafés","shortName":"Café","icon":{"prefix":"https:\/\/foursquare.com\/img\/categories_v2\/food\/cafe_","suffix":".png"},"primary":true}],"verified":false,"restricted":true,"stats":{"checkinsCount":1376,"usersCount":603,"tipCount":18},"specials":{"count":0,"items":[]},"hereNow":{"count":0,"groups":[]},"referralId":"v-1367155543"},{"id":"5111ca2fe4b0159391ea0c7d","name":"Bonifacio Technology Center","contact":{},"location":{"address":"31st Street cor 2nd Avenue","lat":14.554498593208573,"lng":121.04463247805131,"distance":77,"city":"Bonifacio Global City, Taguig","country":"Philippines","cc":"PH"},"canonicalUrl":"https:\/\/foursquare.com\/v\/bonifacio-technology-center\/5111ca2fe4b0159391ea0c7d","categories":[{"id":"4bf58dd8d48988d130941735","name":"Building","pluralName":"Buildings","shortName":"Building","icon":{"prefix":"https:\/\/foursquare.com\/img\/categories_v2\/building\/default_","suffix":".png"},"primary":true}],"verified":false,"restricted":true,"stats":{"checkinsCount":156,"usersCount":53,"tipCount":1},"specials":{"count":0,"items":[]},"hereNow":{"count":0,"groups":[]},"referralId":"v-1367155543"},{"id":"4cd230616449a093c299d4cf","name":"NEC Telecom Software Philippines","contact":{},"location":{"address":"2F Bonifacio Technology Center, 31st cor. 2nd Ave.","lat":14.554701222029573,"lng":121.0440949965463,"distance":72,"city":"Taguig City","country":"Philippines","cc":"PH"},"canonicalUrl":"https:\/\/foursquare.com\/v\/nec-telecom-software-philippines\/4cd230616449a093c299d4cf","categories":[{"id":"4bf58dd8d48988d124941735","name":"Office","pluralName":"Offices","shortName":"Corporate \/ Office","icon":{"prefix":"https:\/\/foursquare.com\/img\/categories_v2\/building\/default_","suffix":".png"},"primary":true}],"verified":false,"restricted":true,"stats":{"checkinsCount":1435,"usersCount":51,"tipCount":3},"specials":{"count":0,"items":[]},"hereNow":{"count":0,"groups":[]},"referralId":"v-1367155543"},{"id":"4e3768ed6284fcf7399bf730","name":"Makati Development Corp. (MDC)","contact":{},"location":{"address":"Bonifacio Technology Center","crossStreet":"31st Street (2nd avenue)","lat":14.554893282917398,"lng":121.04435254006664,"distance":39,"city":"Taguig City","state":"Metro Manila","country":"Philippines","cc":"PH"},"canonicalUrl":"https:\/\/foursquare.com\/v\/makati-development-corp-mdc\/4e3768ed6284fcf7399bf730","categories":[],"verified":false,"restricted":true,"stats":{"checkinsCount":138,"usersCount":40,"tipCount":0},"specials":{"count":0,"items":[]},"hereNow":{"count":0,"groups":[]},"referralId":"v-1367155543"},{"id":"4d5146389d49370435d2ca39","name":"Garage 88 Diner","contact":{},"location":{"address":"Karport, 32nd Street","crossStreet":"Bonifacio Global City","lat":14.55473,"lng":121.0452978,"distance":95,"city":"Taguig","country":"Philippines","cc":"PH"},"canonicalUrl":"https:\/\/foursquare.com\/v\/garage-88-diner\/4d5146389d49370435d2ca39","categories":[{"id":"4bf58dd8d48988d147941735","name":"Diner","pluralName":"Diners","shortName":"Diner","icon":{"prefix":"https:\/\/foursquare.com\/img\/categories_v2\/food\/diner_","suffix":".png"},"primary":true}],"verified":false,"restricted":true,"stats":{"checkinsCount":2152,"usersCount":1054,"tipCount":18},"specials":{"count":0,"items":[]},"hereNow":{"count":0,"groups":[]},"referralId":"v-1367155543"},{"id":"4cdb99903f6a8cfa2948ecea","name":"Grand Hamptons Tower II","contact":{},"location":{"lat":14.55441893387423,"lng":121.04433574376688,"distance":88,"city":"Taguig","state":"Metro Manila","country":"Philippines","cc":"PH"},"canonicalUrl":"https:\/\/foursquare.com\/v\/grand-hamptons-tower-ii\/4cdb99903f6a8cfa2948ecea","categories":[{"id":"4bf58dd8d48988d130941735","name":"Building","pluralName":"Buildings","shortName":"Building","icon":{"prefix":"https:\/\/foursquare.com\/img\/categories_v2\/building\/default_","suffix":".png"},"primary":true}],"verified":false,"restricted":true,"stats":{"checkinsCount":1834,"usersCount":255,"tipCount":4},"specials":{"count":0,"items":[]},"hereNow":{"count":0,"groups":[]},"referralId":"v-1367155543"},{"id":"500e280be4b08b8aba1cc9e0","name":"Rm.738 Global City Innovative College","contact":{},"location":{"lat":14.554817430787585,"lng":121.04439733018789,"distance":44,"city":"Taguig","country":"Philippines","cc":"PH"},"canonicalUrl":"https:\/\/foursquare.com\/v\/rm738-global-city-innovative-college\/500e280be4b08b8aba1cc9e0","categories":[{"id":"4bf58dd8d48988d1a0941735","name":"College Classroom","pluralName":"College Classrooms","shortName":"Classroom","icon":{"prefix":"https:\/\/foursquare.com\/img\/categories_v2\/education\/classroom_","suffix":".png"},"primary":true}],"verified":false,"restricted":true,"stats":{"checkinsCount":73,"usersCount":8,"tipCount":0},"specials":{"count":0,"items":[]},"hereNow":{"count":0,"groups":[]},"referralId":"v-1367155543"},{"id":"4e03c913c65b59b611d8acec","name":"8F Net Plaza Sleeping Quarters","contact":{},"location":{"address":"8th Floor Net Plaza","lat":14.555122393313559,"lng":121.04439733018789,"distance":17,"city":"Taguig City","state":"Metro Manila","country":"Philippines","cc":"PH"},"canonicalUrl":"https:\/\/foursquare.com\/v\/8f-net-plaza-sleeping-quarters\/4e03c913c65b59b611d8acec","categories":[{"id":"4bf58dd8d48988d174941735","name":"Coworking Space","pluralName":"Coworking Spaces","shortName":"Coworking Space","icon":{"prefix":"https:\/\/foursquare.com\/img\/categories_v2\/building\/office_coworkingspace_","suffix":".png"},"primary":true}],"verified":false,"restricted":true,"stats":{"checkinsCount":13,"usersCount":7,"tipCount":0},"specials":{"count":0,"items":[]},"hereNow":{"count":0,"groups":[]},"referralId":"v-1367155543"},{"id":"4eb653fc9a52ad1e3f54b9b0","name":"JP Morgan Chase 16th Floor Net Plaza","contact":{},"location":{"crossStreet":"31st Street Bonifacio Global City","lat":14.555413948967699,"lng":121.0437478718001,"distance":89,"city":"Taguig","country":"Philippines","cc":"PH"},"canonicalUrl":"https:\/\/foursquare.com\/v\/jp-morgan-chase-16th-floor-net-plaza\/4eb653fc9a52ad1e3f54b9b0","categories":[{"id":"4bf58dd8d48988d174941735","name":"Coworking Space","pluralName":"Coworking Spaces","shortName":"Coworking Space","icon":{"prefix":"https:\/\/foursquare.com\/img\/categories_v2\/building\/office_coworkingspace_","suffix":".png"},"primary":true}],"verified":false,"restricted":true,"stats":{"checkinsCount":418,"usersCount":35,"tipCount":0},"specials":{"count":0,"items":[]},"hereNow":{"count":0,"groups":[]},"referralId":"v-1367155543"},{"id":"4fbae103e4b03e1f0d612efe","name":"Mobext Philippines","contact":{},"location":{"address":"301D, 3rd floor, bonifacio Technology Center, 31st Street","crossStreet":"2nd Street","lat":14.554813667918056,"lng":121.04435254006664,"distance":46,"city":"Bonifacio Global City, Taguig","country":"Philippines","cc":"PH"},"canonicalUrl":"https:\/\/foursquare.com\/v\/mobext-philippines\/4fbae103e4b03e1f0d612efe","categories":[{"id":"4bf58dd8d48988d125941735","name":"Tech Startup","pluralName":"Tech Startups","shortName":"Tech Startup","icon":{"prefix":"https:\/\/foursquare.com\/img\/categories_v2\/shops\/technology_","suffix":".png"},"primary":true}],"verified":false,"restricted":true,"stats":{"checkinsCount":35,"usersCount":6,"tipCount":0},"specials":{"count":0,"items":[]},"hereNow":{"count":0,"groups":[]},"referralId":"v-1367155543"},{"id":"50aa1c3de4b0702a837747d4","name":"Shrimp Bucket","contact":{"phone":"+6325502898","formattedPhone":"+63 2 550 2898"},"location":{"address":"Ground Flr, Grand Hampton","crossStreet":"at 2nd Ave & 31st St","lat":14.55421774265474,"lng":121.04497400151189,"distance":117,"city":"Taguig City","state":"Metro Manila","country":"Philippines","cc":"PH"},"canonicalUrl":"https:\/\/foursquare.com\/v\/shrimp-bucket\/50aa1c3de4b0702a837747d4","categories":[{"id":"4bf58dd8d48988d1ce941735","name":"Seafood Restaurant","pluralName":"Seafood Restaurants","shortName":"Seafood","icon":{"prefix":"https:\/\/foursquare.com\/img\/categories_v2\/food\/seafood_","suffix":".png"},"primary":true}],"verified":false,"restricted":true,"stats":{"checkinsCount":289,"usersCount":146,"tipCount":5},"specials":{"count":0,"items":[]},"hereNow":{"count":0,"groups":[]},"referralId":"v-1367155543"},{"id":"4db7b8444b22f2ddb62ce8f3","name":"BIR Taguig RDO 044","contact":{},"location":{"lat":14.554588733314388,"lng":121.04493481034423,"distance":78,"city":"Taguig City","state":"NCR","country":"Philippines","cc":"PH"},"canonicalUrl":"https:\/\/foursquare.com\/v\/bir-taguig-rdo-044\/4db7b8444b22f2ddb62ce8f3","categories":[{"id":"4bf58dd8d48988d126941735","name":"Government Building","pluralName":"Government Buildings","shortName":"Government","icon":{"prefix":"https:\/\/foursquare.com\/img\/categories_v2\/building\/government_","suffix":".png"},"primary":true}],"verified":false,"restricted":true,"stats":{"checkinsCount":131,"usersCount":96,"tipCount":3},"specials":{"count":0,"items":[]},"hereNow":{"count":0,"groups":[]},"referralId":"v-1367155543"},{"id":"4d8c16839f3fb1f7813500bd","name":"Orense Residence","contact":{},"location":{"address":"Panay St.,","lat":14.554846,"lng":121.044516,"distance":38,"postalCode":"1213","city":"Makati City","state":"NCR","country":"Philippines","cc":"PH"},"canonicalUrl":"https:\/\/foursquare.com\/v\/orense-residence\/4d8c16839f3fb1f7813500bd","categories":[],"verified":false,"restricted":true,"stats":{"checkinsCount":29,"usersCount":2,"tipCount":0},"specials":{"count":0,"items":[]},"hereNow":{"count":0,"groups":[]},"referralId":"v-1367155543"},{"id":"4f82e2b5e4b020c7c0378a82","name":"Tips 'N Toes","contact":{},"location":{"address":"Grand Hamptons II","crossStreet":"31st cor. 1st ave.","lat":14.554664291423414,"lng":121.04413978676328,"distance":73,"city":"Bonifacio Global City, Taguig","country":"Philippines","cc":"PH"},"canonicalUrl":"https:\/\/foursquare.com\/v\/tips-n-toes\/4f82e2b5e4b020c7c0378a82","categories":[{"id":"4f04aa0c2fb6e1c99f3db0b8","name":"Nail Salon","pluralName":"Nail Salons","shortName":"Nail Salon","icon":{"prefix":"https:\/\/foursquare.com\/img\/categories_v2\/shops\/nailsalon_","suffix":".png"},"primary":true}],"verified":false,"restricted":true,"stats":{"checkinsCount":172,"usersCount":67,"tipCount":3},"specials":{"count":0,"items":[]},"hereNow":{"count":0,"groups":[]},"referralId":"v-1367155543"},{"id":"50a1a5d8e4b0ed37b3dc7711","name":"Rm.739 Global City Innovative College","contact":{},"location":{"lat":14.555319810889266,"lng":121.04426855854442,"distance":33,"country":"Philippines","cc":"PH"},"canonicalUrl":"https:\/\/foursquare.com\/v\/rm739-global-city-innovative-college\/50a1a5d8e4b0ed37b3dc7711","categories":[{"id":"4bf58dd8d48988d1a0941735","name":"College Classroom","pluralName":"College Classrooms","shortName":"Classroom","icon":{"prefix":"https:\/\/foursquare.com\/img\/categories_v2\/education\/classroom_","suffix":".png"},"primary":true}],"verified":false,"restricted":true,"stats":{"checkinsCount":39,"usersCount":4,"tipCount":0},"specials":{"count":0,"items":[]},"hereNow":{"count":0,"groups":[]},"referralId":"v-1367155543"},{"id":"511050c9e4b0162f3d3ba786","name":"The Sandwich Guy","contact":{},"location":{"lat":14.555352863688032,"lng":121.04416218186556,"distance":45,"country":"Philippines","cc":"PH"},"canonicalUrl":"https:\/\/foursquare.com\/v\/the-sandwich-guy\/511050c9e4b0162f3d3ba786","categories":[{"id":"4bf58dd8d48988d1c5941735","name":"Sandwich Place","pluralName":"Sandwich Places","shortName":"Sandwiches","icon":{"prefix":"https:\/\/foursquare.com\/img\/categories_v2\/food\/sandwiches_","suffix":".png"},"primary":true}],"verified":false,"restricted":true,"stats":{"checkinsCount":4,"usersCount":3,"tipCount":0},"specials":{"count":0,"items":[]},"hereNow":{"count":0,"groups":[]},"referralId":"v-1367155543"},{"id":"4d3d6f19fb4c54813dd4dc14","name":"Karport","contact":{},"location":{"address":"31st St","lat":14.555062567660519,"lng":121.04476124930589,"distance":27,"city":"Taguig City","state":"Metro Manila","country":"Philippines","cc":"PH"},"canonicalUrl":"https:\/\/foursquare.com\/v\/karport\/4d3d6f19fb4c54813dd4dc14","categories":[{"id":"4bf58dd8d48988d1e1931735","name":"Arcade","pluralName":"Arcades","shortName":"Arcade","icon":{"prefix":"https:\/\/foursquare.com\/img\/categories_v2\/arts_entertainment\/arcade_","suffix":".png"},"primary":true}],"verified":false,"restricted":true,"stats":{"checkinsCount":21,"usersCount":14,"tipCount":0},"specials":{"count":0,"items":[]},"hereNow":{"count":0,"groups":[]},"referralId":"v-1367155543"},{"id":"5135b9d1e4b0a9c8f5cca7f0","name":"My Tea","contact":{},"location":{"address":"Unit-D, Sapphire Residences","crossStreet":"2nd Avenue Corner 31st St West Global","lat":14.554248993776376,"lng":121.04473885431496,"distance":106,"city":"Taguig City","country":"Philippines","cc":"PH"},"canonicalUrl":"https:\/\/foursquare.com\/v\/my-tea\/5135b9d1e4b0a9c8f5cca7f0","categories":[{"id":"4bf58dd8d48988d1dc931735","name":"Tea Room","pluralName":"Tea Rooms","shortName":"Tea Room","icon":{"prefix":"https:\/\/foursquare.com\/img\/categories_v2\/food\/tearoom_","suffix":".png"},"primary":true}],"verified":false,"restricted":true,"stats":{"checkinsCount":35,"usersCount":25,"tipCount":1},"specials":{"count":0,"items":[]},"hereNow":{"count":0,"groups":[]},"referralId":"v-1367155543"},{"id":"4d116fe2f6f3721e717a94e9","name":"Meguiar's VIP Auto Salon","contact":{},"location":{"address":"Bonifacio Global City","lat":14.554662958503288,"lng":121.04520354952331,"distance":91,"city":"Taguig City","state":"Metro Manila","country":"Philippines","cc":"PH"},"canonicalUrl":"https:\/\/foursquare.com\/v\/meguiars-vip-auto-salon\/4d116fe2f6f3721e717a94e9","categories":[],"verified":false,"restricted":true,"stats":{"checkinsCount":83,"usersCount":28,"tipCount":3},"specials":{"count":0,"items":[]},"hereNow":{"count":0,"groups":[]},"referralId":"v-1367155543"},{"id":"4dc96743d4c0abe9b62ed8ac","name":"UCPB","contact":{},"location":{"address":"Ground Floor, Fort Palm Spring Building, 30th street corner 1st street","lat":14.554683128436094,"lng":121.0442304611206,"distance":65,"city":"Taguig City","state":"Metro Manila","country":"Philippines","cc":"PH"},"canonicalUrl":"https:\/\/foursquare.com\/v\/ucpb\/4dc96743d4c0abe9b62ed8ac","categories":[{"id":"4bf58dd8d48988d124941735","name":"Office","pluralName":"Offices","shortName":"Corporate \/ Office","icon":{"prefix":"https:\/\/foursquare.com\/img\/categories_v2\/building\/default_","suffix":".png"},"primary":true}],"verified":false,"restricted":true,"stats":{"checkinsCount":34,"usersCount":13,"tipCount":0},"specials":{"count":0,"items":[]},"hereNow":{"count":0,"groups":[]},"referralId":"v-1367155543"},{"id":"4f1423d7d5fb04999e8c9a58","name":"IP Converge Bonifacio Global City","contact":{},"location":{"address":"Bonifacio Technology Center, 31st St. cor 2nd Ave., Bonifacio Global City","lat":14.555000894788334,"lng":121.04391932487488,"distance":70,"city":"Taguig City","country":"Philippines","cc":"PH"},"canonicalUrl":"https:\/\/foursquare.com\/v\/ip-converge-bonifacio-global-city\/4f1423d7d5fb04999e8c9a58","categories":[{"id":"4bf58dd8d48988d174941735","name":"Coworking Space","pluralName":"Coworking Spaces","shortName":"Coworking Space","icon":{"prefix":"https:\/\/foursquare.com\/img\/categories_v2\/building\/office_coworkingspace_","suffix":".png"},"primary":true}],"verified":false,"restricted":true,"stats":{"checkinsCount":104,"usersCount":11,"tipCount":0},"specials":{"count":0,"items":[]},"hereNow":{"count":0,"groups":[]},"referralId":"v-1367155543"},{"id":"51367f58e4b05751bb46c758","name":"jacky's canteen","contact":{},"location":{"lat":14.554559118606202,"lng":121.04408379898948,"distance":86,"country":"Philippines","cc":"PH"},"canonicalUrl":"https:\/\/foursquare.com\/v\/jackys-canteen\/51367f58e4b05751bb46c758","categories":[{"id":"4eb1bd1c3b7b55596b4a748f","name":"Filipino Restaurant","pluralName":"Filipino Restaurants","shortName":"Filipino","icon":{"prefix":"https:\/\/foursquare.com\/img\/categories_v2\/food\/default_","suffix":".png"},"primary":true}],"verified":false,"restricted":true,"stats":{"checkinsCount":12,"usersCount":1,"tipCount":0},"specials":{"count":0,"items":[]},"hereNow":{"count":0,"groups":[]},"referralId":"v-1367155543"},{"id":"4ffb8a3ae4b063a95decf43b","name":"31st 2nd Ave","contact":{},"location":{"lat":14.555393544203481,"lng":121.04408939776802,"distance":54,"country":"Philippines","cc":"PH"},"canonicalUrl":"https:\/\/foursquare.com\/v\/31st-2nd-ave\/4ffb8a3ae4b063a95decf43b","categories":[{"id":"4bf58dd8d48988d130941735","name":"Building","pluralName":"Buildings","shortName":"Building","icon":{"prefix":"https:\/\/foursquare.com\/img\/categories_v2\/building\/default_","suffix":".png"},"primary":true}],"verified":false,"restricted":true,"stats":{"checkinsCount":23,"usersCount":13,"tipCount":0},"specials":{"count":0,"items":[]},"hereNow":{"count":0,"groups":[]},"referralId":"v-1367155543"},{"id":"511a5e59e4b080326f96c630","name":"Harvest, The Fort","contact":{},"location":{"lat":14.554565487053566,"lng":121.04439733018789,"distance":71,"country":"Philippines","cc":"PH"},"canonicalUrl":"https:\/\/foursquare.com\/v\/harvest-the-fort\/511a5e59e4b080326f96c630","categories":[{"id":"4bf58dd8d48988d14e941735","name":"American Restaurant","pluralName":"American Restaurants","shortName":"American","icon":{"prefix":"https:\/\/foursquare.com\/img\/categories_v2\/food\/default_","suffix":".png"},"primary":true}],"verified":false,"restricted":true,"stats":{"checkinsCount":12,"usersCount":11,"tipCount":0},"specials":{"count":0,"items":[]},"hereNow":{"count":0,"groups":[]},"referralId":"v-1367155543"},{"id":"50177363e4b0aa0aea3306dd","name":"Global City Innovative College ( Library )","contact":{},"location":{"lat":14.554698,"lng":121.043983,"distance":81,"city":"Taguig","country":"Philippines","cc":"PH"},"canonicalUrl":"https:\/\/foursquare.com\/v\/global-city-innovative-college--library-\/50177363e4b0aa0aea3306dd","categories":[{"id":"4bf58dd8d48988d1a7941735","name":"College Library","pluralName":"College Libraries","shortName":"Library","icon":{"prefix":"https:\/\/foursquare.com\/img\/categories_v2\/building\/library_","suffix":".png"},"primary":true}],"verified":false,"restricted":true,"stats":{"checkinsCount":110,"usersCount":15,"tipCount":0},"specials":{"count":0,"items":[]},"hereNow":{"count":0,"groups":[]},"referralId":"v-1367155543"},{"id":"4f6a123ae4b070a75289cd24","name":"Grand Hamptons Tower I","contact":{},"location":{"lat":14.554382810142785,"lng":121.04453170045176,"distance":89,"country":"Philippines","cc":"PH"},"canonicalUrl":"https:\/\/foursquare.com\/v\/grand-hamptons-tower-i\/4f6a123ae4b070a75289cd24","categories":[{"id":"4bf58dd8d48988d130941735","name":"Building","pluralName":"Buildings","shortName":"Building","icon":{"prefix":"https:\/\/foursquare.com\/img\/categories_v2\/building\/default_","suffix":".png"},"primary":true}],"verified":false,"restricted":true,"stats":{"checkinsCount":130,"usersCount":36,"tipCount":2},"specials":{"count":0,"items":[]},"hereNow":{"count":0,"groups":[]},"referralId":"v-1367155543"}]	
}

// Global variables
var f4sqr = require('foursquare');
var animation  = require('alloy/animation');
var lat = 0.0, lon = 0.0;
var SELECTED_CATEGORY_ID = "";

Titanium.include("version.js");
Titanium.Geolocation.preferredProvider = "gps";

// JOHN: Location services check up
if(Ti.Geolocation.locationServicesEnabled === false){
	//alert(" Please activate location services for your device");
} else {
	if(Ti.Geolocation.locationServicesAuthorization == 
		Ti.Geolocation.AUTHORIZATION_AUTHORIZED){

		if (isIPhone3_2_Plus())
		{
		    //NOTE: starting in 3.2+, you'll need to set the applications
		    //purpose property for using Location services on iPhone
		    Titanium.Geolocation.purpose = "GPS demo";
		}

		Titanium.Geolocation.accuracy = Titanium.Geolocation.ACCURACY_KILOMETER;
		Titanium.Geolocation.distanceFilter = 30;
		Titanium.Geolocation.getCurrentPosition(function(e) {

			if(Ti.Platform.model === 'Simulator' || Ti.Platform.model.indexOf('sdk') !== -1 ) {
		    	// for demo, staticly define latlon coordinates
				lon = 121.044547; 
				lat = 14.555189; 
		    } else {

		    	lon = e.coords.longitude;
		        lat = e.coords.latitude;
		    }

		    var RADIUS_IN_METERS = 590;
		    var categoryId = SELECTED_CATEGORY_ID;
		    var category = '&categoryId=' +  categoryId ;
		    var radius = '&intent=browse&radius=' + RADIUS_IN_METERS;
			var criteria = (categoryId !== '') ? '?ll=' + lat + ',' + lon + category + radius 
							: '?ll=' + lat + ',' + lon + radius;

			// populate our mapView
			f4sqr.get_venue(Alloy.Globals.F4SQR_CREDS, criteria, function(e){

				// Let's mock this for the mean time 
				//var result = (e) ? e : Alloy.Globals.GBC_F4SQR_DATA;
				var result = e;
				Alloy.Globals.VENUE_DATA = result;

				// Fire an event with necessary data to our mapView
				Titanium.App.fireEvent('loadMapView', {
					lat: lat,
					lon: lon,
					venues: result
				});

				Titanium.App.fireEvent('loadListView', {
					venues: result
				});

			});
		});

		// JOHN : Decrease the timeout to this amount.
		TIMEOUT = 500;
		Alloy.Globals.BGC_MANUALLY_SET_LOCATION = false;
	} else {
		Alloy.Globals.BGC_MANUALLY_SET_LOCATION = true;
	}
}
