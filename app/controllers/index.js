
if (OS_IOS && Alloy.isHandheld) {
	Alloy.Globals.navgroup = $.navgroup;	
}

setTimeout(function() {
	if(Alloy.Globals.BGC_MANUALLY_SET_LOCATION){
		$.master.getView().fireEvent('ASK_LOCATION');
	}

	if (OS_ANDROID) {
		$.master.getView().open({});
	} else {
		$.index.open({
		    transition:Titanium.UI.iPhone.AnimationStyle.FLIP_FROM_LEFT,
		});
	}
}, TIMEOUT);
