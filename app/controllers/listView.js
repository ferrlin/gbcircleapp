var args = arguments[0] || {},
    pulling = false,
    reloading = false,
    offset = 0;

$.table.addEventListener('click', function(e){
    $.trigger('detail',e);
});

$.table.addEventListener('scroll', function(e){
    offset = e.contentOffset.y;
    if (pulling && !reloading && offset > -80 && offset < 0){
        pulling = false;
        var unrotate = Ti.UI.create2DMatrix();
        //imageArrow.animate({transform:unrotate, duration:180});
        labelStatus.text = 'Pull down to refresh...';

    } else if (!pulling && !reloading && offset < -80){
        pulling = true;
        var rotate = Ti.UI.create2DMatrix().rotate(180);
        //imageArrow.animate({transform:rotate, duration:180});
        labelStatus.text = 'Release to refresh...';
    }
});


Titanium.App.addEventListener('loadListView', function(e){
    
    // alert("From listView.js : " + JSON.stringify(e));

    loadTableWithData($.table, e.venues, function(event){
        $.table.scrollToTop(0);
        //alert("This table is called " + $.table);
    });
});

/** - Search functionality end -  */
var tableHeader = Ti.UI.createView({
    backgroundColor:'#e2e7ed',
    width:320, 
    height:60
});
 
var imageArrow = Ti.UI.createImageView({
    image:'https://github.com/appcelerator/titanium_mobile/raw/master/demos/KitchenSink/Resources/images/whiteArrow.png',
    left:20, bottom:10,
    width:23, height:60
});
//tableHeader.add(imageArrow);
 
var labelStatus = Ti.UI.createLabel({
    color:'#009cab',
    font:{
        fontSize:13, 
        fontWeight:'bold',
        fontFamily : "Century Gothic"
    },
    text:'Pull down to refresh...',
    textAlign:'center',
    left:55,
    bottom:30,
    width:200
});
tableHeader.add(labelStatus);
 
var labelLastUpdated = Ti.UI.createLabel({
    color:'#009cab',
    font:{fontSize:12},
    text:'Last Updated: ' + getFormattedDate(),
    textAlign:'center',
    left:55, bottom:15,
    width:200
});
tableHeader.add(labelLastUpdated);
 
var actInd = Ti.UI.createActivityIndicator({
    left:20, bottom:13,
    width:30, height:30
});
tableHeader.add(actInd);

$.table.headerPullView = tableHeader;
var tableRowTotal = 0;

function loadTableWithData(table, venues, callback){

    /*if(data.length > 0){
        data = [];
    }*/
    var data = [];

    _.each(venues, function(item, name){

        data.push(Alloy.createController('row', {
            name: item.name,
            location: item.location,
            click: function(e){
                 // get the detail controller and window references
                var controller = OS_IOS && Alloy.isTablet ? $.detail : Alloy.createController('detail');
                var win = controller.getView();
                
                // WorkAround : let's pass the index of the row here..
                controller.setInfo(e.index);

                // open the detail windows 
                if (OS_IOS && Alloy.isHandheld) {
                    Alloy.Globals.navgroup.open(win);   
                } else if (OS_ANDROID) {
                    win.open();
                } 
            }
        }).getView());
    });

    // Set the data to the table.
    table.setData(data);

    if(callback && typeof callback === "function"){
        callback(table);
    }
}

/**  - Search for list view - */
$.search.addEventListener('return', function(e){
    if(! Titanium.Network.online ){
        alert("You need an internet connection to search for books.");
    }
    else if(e.value.length >= 3){

        var RADIUS_IN_METERS = 590;
        var categoryId = '4bf58dd8d48988d1c4941735';
        var category = '&categoryId=' +  categoryId ;
        var radius = '&intent=browse&radius=' + RADIUS_IN_METERS;

        // based on user given keywords
        var query = '&query=' + e.value;

        var criteria = '?ll=' + lat + ',' + lon + query + category + radius;
        f4sqr.get_venue(Alloy.Globals.F4SQR_CREDS, criteria, function(e){
            var result = e;
            Alloy.Globals.VENUE_DATA = result;
            Titanium.App.fireEvent('loadMapView', {
                lat: lat,
                lon: lon,
                venues: result
            });
            
            Titanium.App.fireEvent('loadListView', {
                venues: result
            });
        });
        
    } 
    else {
        alert("Please use minimum 3 characters to search.");
    }
});

$.table.addEventListener('dragEnd',function(e){
    if (pulling && !reloading && offset < -80){
        pulling = false;
        reloading = true;
        labelStatus.text = 'Updating...';
        //imageArrow.hide();
        //actInd.show();
        e.source.setContentInsets({top:80}, {animated:true});

        var RADIUS_IN_METERS = 590;
        var categoryId = SELECTED_CATEGORY_ID;
        var category = '&categoryId=' +  categoryId ;
        var radius = '&intent=browse&radius=' + RADIUS_IN_METERS;

        // based on user given keywords
        var query = '&query=' + $.search.value;

        var criteria = ($.search.value === undefined) ? '?ll=' + lat + ',' + lon + radius 
                        : '?ll=' + lat + ',' + lon + query + radius;

        if(categoryId !== ''){
            criteria += category;
        }


        f4sqr.get_venue(Alloy.Globals.F4SQR_CREDS, criteria, function(e){
            var result = e;
            Alloy.Globals.VENUE_DATA = result;

            Titanium.App.fireEvent('loadMapView', {
                lat: lat,
                lon: lon,
                venues: result
            },resetPullHeader($.table));

            Titanium.App.fireEvent('loadListView',{
                venues: result
            });
        });

    }
});

$.table.addEventListener('dragStart', function(e){
    $.search.blur();
});



function resetPullHeader(table){
    reloading = false;
    labelLastUpdated.text = 'Last Updated: ' + getFormattedDate();
    actInd.hide();
    //imageArrow.transform=Ti.UI.create2DMatrix();
    //imageArrow.show();
    labelStatus.text = 'Pull down to refresh...';
    table.setContentInsets({top:0}, {animated:true});
}


function getFormattedDate(){
    var date = new Date();
    return date.getMonth() + '/' + date.getDate() + '/' + date.getFullYear() + ' ' + date.getHours() + ':' + date.getMinutes();
}