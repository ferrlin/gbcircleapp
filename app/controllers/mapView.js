/*var args = arguments[0] || {},
    data = [];*/

$.mapView.addEventListener("focus", function(e){

    if(Ti.Geolocation.locationServicesEnabled){
        Ti.Geolocation.getCurrentPosition(function(e){

            if(!e.success || e.error){
                currentLocation.text = 'error: ' + JSON.stringify(e.error);
                    Ti.API.info("Code translation: "+translateErrorCode(e.code));
                    alert('error ' + JSON.stringify(e.error));
                    return;
            }

            var _longitude = e.coords.longitude;
            var _latitude = e.coords.latitude;
            var altitude = e.coords.altitude;
            var heading = e.coords.heading;
            var accuracy = e.coords.accuracy;
            var speed = e.coords.speed;
            var timestamp = e.coords.timestamp;
            var altitudeAccuracy = e.coords.altitudeAccuracy;


            Ti.API.info('speed ' + speed);
            //currentLocation.text = 'long:' + longitude + ' lat: ' + latitude;
    
            Titanium.API.info('geo - current location: ' + new Date(timestamp) + ' long ' + _longitude + ' lat ' + _latitude + ' accuracy ' + accuracy);
           
            $.mapView.mapType =  MapModule.NORMAL_TYPE;
            $.mapView.setRegion({
                latitude: _latitude,
                longtitude: _longtitude,
                latitudeDelta: 0.01,
                longitudeDelta: 0.01
            });
            $.mapView.animate = true;

        });
    } else {
        alert('Please enabled location services.');
    }
});


Titanium.App.addEventListener('loadMapView',function(e){

    /*(if(data.length > 0){
        data = [];
    }

    // John: Loading the list view through this code statement.
    _.each(e.venues, function(item, name){

        data.push(Alloy.createController('row', {
            name: item.name,
            location: item.location
        }).getView());
    });*/
    
    /*loadTableWithData($.table, data);*/
    initMapView(e.lat, e.lon, e.venues);
});

function initMapView(lat, lon, venues){

    var annotatedVenues = [];
    var avatar  = Ti.UI.createButton({
        backgroundImage: 'http://i817.photobucket.com/albums/zz96/dialce01/GooglePlacesIcon2x.png',
    }); 

    for(var index = 0; index < venues.length ;  index++){

        var annotate = Ti.Map.createAnnotation({
            thisId: venues[index].id,
            latitude:venues[index].location.lat,
            longitude:venues[index].location.lng,
            pinColor:Titanium.Map.ANNOTATION_GREEN,
            title: venues[index].name,
            subtitle: venues[index].location.address,
            animate:true,
            rightButton : Ti.UI.iPhone.SystemButton.DISCLOSURE
        });

        annotatedVenues.push(annotate);
    }

    $.mapView.mapType =  Ti.Map.HYBRID_TYPE;
    $.mapView.setLocation({
        latitude: lat,
        longitude: lon,
        latitudeDelta: 0.0015,
        longitudeDelta: 0.0015
    });

    $.mapView.setAnnotations(annotatedVenues);
    $.mapView.setAnimate(true);
}