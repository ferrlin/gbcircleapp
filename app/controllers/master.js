var isViewingMap = true,
    data = [];

// JOHN: This should only be visible for manually set location
$.locationBackground.visible = Alloy.Globals.BGC_MANUALLY_SET_LOCATION;

/*
    JOHN: Allows us to segue from list view to detail view.
*/
$.table.on('detail', function(e) {

    // get the detail controller and window references
    var controller = OS_IOS && Alloy.isTablet ? $.detail : Alloy.createController('detail');
    var win = controller.getView();
    
    // WorkAround : let's pass the index of the row here..
    controller.setInfo(e.index);

    // open the detail windows 
    if (OS_IOS && Alloy.isHandheld) {
        Alloy.Globals.navgroup.open(win);   
    } else if (OS_ANDROID) {
        win.open();
    } 
});

$.master_win.addEventListener('ASK_LOCATION',function(e){
    setTimeout(function(e){
        $.supplyLocation.animate({
            top:0,
            duration: 500
        });
    },1000);
});

// Mapping service
if (Titanium.Geolocation.locationServicesEnabled === false)
{
    Titanium.UI.createAlertDialog({title:'Kitchen Sink', message:'Your device has geo turned off - turn it on.'}).show();
} else {
    var win = $.master_win;
    if (Titanium.Platform.name != 'android') {
        if(win.openedflag == 0 ){
            Ti.API.info('firing open event');
            win.fireEvent('open');
        }
        if(win.focusedflag == 0){
            Ti.API.info('firing focus event');
            win.fireEvent('focus');
        }
        var authorization = Titanium.Geolocation.locationServicesAuthorization;
        Ti.API.info('Authorization: '+authorization);
        if (authorization == Titanium.Geolocation.AUTHORIZATION_DENIED) {
            Ti.UI.createAlertDialog({
                title:'Kitchen Sink',
                message:'You have disallowed Titanium from running geolocation services.'
            }).show();
        }
        else if (authorization == Titanium.Geolocation.AUTHORIZATION_RESTRICTED) {
            Ti.UI.createAlertDialog({
                title:'Kitchen Sink',
                message:'Your system has disallowed Titanium from running geolocation services.'
            }).show();
        }
    }
}

/** - Search for location lat lon  - */
// $.search.addEventListener('blur', onSearchBlur);
$.locationSearch.addEventListener('return', function(e){
    if ( !Titanium.Network.online ) {
        alert( "You need an internet connection to search for books.");
    }
    else if ( e.value.length >= 3 ) {
        do_search(e.value);
        $.locationSearch.blur();
    }
    else {
        alert("Please use minimum 3 characters to search.");
    }
});

function do_search(location){
    // forward geocode the input
    Titanium.Geolocation.forwardGeocoder(location, function(evt){
        lat = evt.latitude;
        lon = evt.longitude;
        // slide to hide
        setTimeout(function(e){
            $.supplyLocation.animate({
                top:-125,
                duration: 500
            });
        },700);

        $.locationBackground.zIndex = 1;

         // search for nearby places ..
        var RADIUS_IN_METERS = 590;
        var categoryId = '4bf58dd8d48988d1c4941735';
        var category = '&categoryId=' +  categoryId ;
        var radius = '&intent=browse&radius=' + RADIUS_IN_METERS;

        var criteria = ($.search.value === undefined) ? '?ll=' + lat + ',' + lon + category + radius 
                        : '?ll=' + lat + ',' + lon + category + radius;

        f4sqr.get_venue(Alloy.Globals.F4SQR_CREDS, criteria, function(e){
            var result = e;
            Alloy.Globals.VENUE_DATA = result;
            Titanium.App.fireEvent('loadMapView', {
                lat: lat,
                lon: lon,
                venues: result
            }/*,resetPullHeader($.table)*/);
        });

    });
}

// $.toggleButton.visible = false;

// $.toggleButton.addEventListener('click', function(e) {
// 	if(isViewingMap){
// 		$.toggleButton.title = 'Map';
// 		$.mapView.animate({
// 			view:$.table,
// 			transition: Ti.UI.iPhone.AnimationStyle.FLIP_FROM_LEFT});
// 	   crossFadeToMapView();
// 	} else {
// 		$.toggleButton.title = 'List';
// 		crossFaceToTableView();
// 	}
// });

function crossFading(from,to,duration,finishCallback){

    from && from.animate({
        opacity: 0,
        duration: duration
    });
    to && to.animate({
        opacity: 1,
        duration: duration
    });
    finishCallback && setTimeout(finishCallback, duration + 300);
    
}

function crossFadeToMapView(){
    //var _mapView = Alloy.createController('')
    var _listView = Alloy.createController('listView').getView();
    var _mapView = Alloy.createController('mapView').getView();
    // animation.crossFade(_mapView,_listView, 500);
    crossFading(_mapView, _listView, 500);
    isViewingMap = false;
}

function crossFaceToTableView(){
    var _listView = Alloy.createController('listView').getView();
    var _mapView = Alloy.createController('mapView').getView();
    //animation.crossFade(_listView, _mapView, 500);
    crossFading(_listView,_mapView,500);
    isViewingMap = true;
}

/** - Start : Path menu controls **/
var pathmenu = require('/pathmenu').createMenu({
	iconList : [
		{ image: 'images/sm/facebook.png', id: 'facebook' },
		{ image: 'images/sm/pridat.png', id: 'pridat' },
		{ image: 'images/sm/twitter.png', id: 'twitter' },
		{ image: 'images/sm/vimeo.png', id: 'vimeo' },
		{ image: 'images/sm/youtube.png', id: 'youtube' }
	],
    buttonImage : 'images/add.png'
});

pathmenu.addEventListener('iconClick', function(e) {
	Ti.API.info(e.source);
	Ti.API.info(e.index);
	Ti.API.info(e.id);
	label.text = 'index: ' + e.index + '\nid: ' + (e.id ? e.id : 'undefined');
});

//$.master_win.add(pathmenu);
/** - End : Adding path menu like controls */




/** Location Background to hide keyboard when locationSearch loses focus **/
$.locationBackground.addEventListener('click', function(e){
    $.locationSearch.blur();
});

/** - Start : Drop Menu **/

var ORIG_BOTTOM = 0;
var MOVE_BY = 230;
var ANIMATION_DURATION = 250;
var hasShownMenu = false;
var touchStarted = false;
var IS_ANDROID = Titanium.Platform.name == 'android';

if(!IS_ANDROID) {

    $.filterMenu.addEventListener('click',function(e){
        toggleMenu($.menu);
    });
    //$.filterMenu.addEventListener('click', toggleMenu($.menu));
}

function toggleMenu(e){

    if (!hasShownMenu) {
        slideOpenDropMenu();
    } else {
        slideCloseDropMenu();
    }
}

function slideOpenDropMenu(){
    $.dropMenu.animate({
        top: 0,
        duration : 500
    });

    hasShownMenu = true;
}

function slideCloseDropMenu(){
    $.dropMenu.animate({
        top: -400,
        duration : 500
    });

    hasShownMenu = false;
}

$.dropMenu.data = Alloy.Globals.BGCApp_CATEGORIES;
$.dropMenu.addEventListener('click', function(e){
    
    Ti.API.info("I selected : " + e.rowData);

    var RADIUS_IN_METERS = 590;
    SELECTED_CATEGORY_ID = e.rowData.id;
    var categoryId = SELECTED_CATEGORY_ID; 
    var category = '&categoryId=' +  categoryId ;
    var radius = '&intent=browse&radius=' + RADIUS_IN_METERS;
    var criteria = (categoryId !== '') ? '?ll=' + lat + ',' + lon + category + radius 
                    : '?ll=' + lat + ',' + lon + radius;

    // populate our mapView
    f4sqr.get_venue(Alloy.Globals.F4SQR_CREDS, criteria, function(e){

        // Let's mock this for the mean time 
        //var result = (e) ? e : Alloy.Globals.GBC_F4SQR_DATA;
        var result = e;
        Alloy.Globals.VENUE_DATA = result;

        // Fire an event with necessary data to our mapView
        Titanium.App.fireEvent('loadMapView', {
            lat: lat,
            lon: lon,
            venues: result
        });

        Titanium.App.fireEvent('loadListView', {
            venues: result
        });
    });

    slideCloseDropMenu();
});


/** - End: Drop Menu- **/

/*$.mapView.addEventListener('click', function(evt){
    
    //is rightbutton clicked?
    if (evt.clicksource == 'rightButton') {

        Titanium.API.info('Right button clicked');

        // get the detail controller and window references
        var controller = OS_IOS && Alloy.isTablet ? $.detail : Alloy.createController('detail');
        var win = controller.getView();
        
        // WorkAround : let's pass the index of the row here..
        controller.setInfo(evt.index);

        // open the detail windows 
        if (OS_IOS && Alloy.isHandheld) {
            Alloy.Globals.navgroup.open(win);   
        } else if (OS_ANDROID) {
            win.open();
        } 
    };
 
    // get the detail controller and window references
    var controller = OS_IOS && Alloy.isTablet ? $.detail : Alloy.createController('detail');
    var win = controller.getView();

    // open the detail windows 
    if (OS_IOS && Alloy.isHandheld) {
        //Alloy.Globals.navgroup.open(win);   
    } else if (OS_ANDROID) {
        win.open();
    } 
});*/