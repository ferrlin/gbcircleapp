var details_image_size = "1080x608";

exports.setInfo = function(index){

    var info = Alloy.Globals.VENUE_DATA[index];

    var criteria = info.id;
    var name = info.name;
    
    f4sqr.get_details(Alloy.Globals.F4SQR_CREDS, criteria, function(e){

        var result = e;

        // index of 7 ; whim
        if(typeof(result.photos.groups) !== "undefined" 
            && result.photos.groups.length > 0) {

            var photos = result.photos.groups[0].items;
            var detailPhotos = [];
        
            if(photos !== undefined){
                for(var index=0; index < photos.length ;  index++){
                    var photo = photos[index];
                    var image = photo.prefix + details_image_size + photo.suffix;
                    var detailsImage = Ti.UI.createImageView({
                        image: image
                    });
                    detailPhotos.push(detailsImage);
                }
                $.scrollView.views = detailPhotos;
            }
        }
 
        // Set Information tab details
        $.address.text = result.location.address;
        $.country.text = result.location.country;
        $.phone.text = result.contact.formattedPhone;
        $.phone.addEventListener('click', function(e){
            Titanium.Platform.openURL('tel:' + result.contact.phone);
        });
       
    })

	if (OS_ANDROID) {
		$.name.text = 'Name: ' + name;
	} else {
		$.placesDetailWindow.title = name;
	}
	$.row_info.top = '40dp';

    // Set Reviews details
    // todo :  this should be defined in detail.js
    
    // Set Promos details
    // todo : ditto

    // Set Menus details
    // todo : ditto

    // Set Directions details
    // todo : ditto

    var spacer = Math.round(Ti.Platform.displayCaps.platformWidth * 0.40);
    var width = spacer - 4;
    var height = 36;

    var headerTab = Ti.UI.createScrollView({
        contentWidth: 'auto',
        contentHeight: 'auto',
        showVerticalScrollIndicator: false,
        showHorizontalScrollIndicator: false,
        backgroundColor : "#009cab",
        width : '80%',
    	height : '40dp'
    });

    // JOHN :  for now, we will not have the menu item for promos, menus since
    // we don't have the data to fill for that.
    //var tabNames = ['Information', 'Promos', 'Reviews', 'Menus', 'Direction'];
    var tabsObject =  {
        'Information' : {

            onclick : function(e){
                e.source.color = "#009cab";
                headerTab.fireEvent('reset_tabs',
                {
                    selectedIndex : e.source.index
                });

                var theSection = $.details.sections[0];
                var rows = [];
                for(var row in theSection.rows){
                    rows.push(theSection.rows[row]);
                }

                for(var index in rows){
                    theSection.remove(rows[index]);
                }

                // JOHN: this is redundant, I'm 
                // just testing.. I might forget to fix this 
                // though. :)
                // theSection.add($.row_info);

                $.details.data = [ theSection ];               
            }
        },
        'Reviews' : {

            onclick : function(e){
                e.source.color = "#009cab";
                headerTab.fireEvent('reset_tabs',
                {
                    selectedIndex : e.source.index
                });

                var theSection = $.details.sections[0];
                var rows = [];
                for(var row in theSection.rows){
                    rows.push(theSection.rows[row]);
                }

                for(var index in rows){
                    theSection.remove(rows[index]);
                }

                var newView = Ti.UI.createView({

                });

                // theSection.add(newView);

                // $.details.data = [ theSection ];  
            }
        },
        'Direction' : {

            onclick : function(e){
                e.source.color = "#009cab";
                headerTab.fireEvent('reset_tabs',
                {
                    selectedIndex : e.source.index
                });

                var theSection = $.details.sections[0];
                var rows = [];
                for(var row in theSection.rows){
                    rows.push(theSection.rows[row]);
                }

                for(var index in rows){
                    theSection.remove(rows[index]);
                }

                var newView = Ti.UI.createView({

                });

                // theSection.add(newView);

                // $.details.data = [ theSection ];  
            }
        }
    };
    var leftOrigin = 2;
    var counter = 0;
    for(var index in tabsObject){
        var position = leftOrigin + (counter * ( width + 3 )),
            tabMark = leftOrigin * (Math.pow(counter,2) * 10);
        
        var tab = Ti.UI.createView({
            width:width,
            height:height,
            left:position,
            borderRadius:2,
            index : counter,
            mark : tabMark
            
        });
        var label = Ti.UI.createLabel({
            text:index,
            color:'#FFF',
            font:{
                fontSize : '18dp',
                fontWeight: 'bold',
                fontFamily: 'Century Gothic'
            },
            index : counter
        });

        var animation = Ti.UI.createAnimation();
        animation.duration = 500;
        animation.bottom = 0;

        var altAnimation = Ti.UI.createAnimation();
        animation.duration = 500;
        animation.left = 0;

        tab.add(label); 
        headerTab.add(tab);

        tab.addEventListener('click', tabsObject[index].onclick);
        counter ++;
    }

    headerTab.addEventListener('click', function(e){
       //alert("the position: "+ JSON.stringify(e.source.width));
       // alert("The width of the scrollView: " + this.left);
       //this.scrollTo(e.x,0);
    });

    headerTab.addEventListener('reset_tabs', function(args){
        for(var index in args.source.children){
            if(index == args.selectedIndex){
                args.source.children[index].backgroundColor = "white";
                args.source.children[index].children[0].color = "#009cab";

                args.source.children[index].onclick;

                // alert("the scrollable region width: " + args.contentWidth);
                var the_x = args.source.children[index].mark;

                // alert("The x used: " + the_x);

                this.scrollTo(the_x,0);

                //alert(args.source.children[index].onclick);
            }
            else{
                args.source.children[index].backgroundColor = "#009cab";
                args.source.children[index].children[0].color = "white";
            }
        }
    });
    
    var menuSection = Ti.UI.createTableViewSection({
    	headerView : headerTab
    });

    menuSection.add($.row_info);
    $.details.data = [ menuSection ];
}