require('behave').andSetup(this);

describe('Testing listing in table view', function(){
	it('Loading data for table view should not exceed 12', function(){
		expect("Hello World").toBe("Betcha by golly wow");
	});
});